require 'test/unit'
require_relative 'atbash'

class AtbashTest < Test::Unit::TestCase
  CONSTANT = 'abcdefghijklmnopqrstuvwxyz'

  def test_decrypt_only_know_chars
    cipher = 'zodvqukgwefbyitmrsplhacxnj'
    encrypted_text = 'dzs'
    assert_equal('car', Atbash.decrypt(cipher, encrypted_text))
  end

  def test_decrypt_with_unknow_chars
    cipher = 'xipmuzfkbrlwotjancqgveshdy'
    encrypted_text = 'skd qj qucbjvq?'
    assert_equal('why so serious?', Atbash.decrypt(cipher, encrypted_text))
  end

  def test_decrypt_challenge
    cipher = 'oephjizkxdawubnytvfglqsrcm'
    encrypted_text = 'knlfgnb, sj koqj o yvnewju'
    assert_equal('houston, we have a problem', Atbash.decrypt(cipher, encrypted_text))
  end

  def test_encypt
    cipher = 'oephjizkxdawubnytvfglqsrcm'
    original_text = 'houston, we have a problem'
    assert_equal('knlfgnb, sj koqj o yvnewju', Atbash.encrypt(cipher, original_text))
  end
end
