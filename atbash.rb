module Atbash
  CONSTANT = 'abcdefghijklmnopqrstuvwxyz'

  def self.encrypt(cipher, str)
    str.tr(CONSTANT, cipher)
  end

  def self.decrypt(cipher, str)
    str.tr(cipher, CONSTANT)
  end
end
