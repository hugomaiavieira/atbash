# Atbash

This is a solution for [atbash challenge](https://gist.github.com/EdwinRozario/f41ab165969f2afce42d388379b26a03).

The solution was written in ruby.

# Usage

``` ruby
require_relative 'atbash'

cipher = 'zodvqukgwefbyitmrsplhacxnj'

# encrypt
original_text = 'car'
Atbash.encrypt(cipher, original_text) # "dzs"

#decrypt
encrypted_text = 'dzs'
Atbash.decrypt(cipher, encrypted_text) # "car"
```

# Running the tests

```
ruby test.rb
```
